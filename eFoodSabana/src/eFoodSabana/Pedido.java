package eFoodSabana;

public class Pedido {

	
	private int precioTotal;
	private String fechaSolicitud, fechaEntrega;
	private Producto producto;
	private boolean estadoEntrega;
	
	
	public Pedido() {
		
		this.precioTotal = precioTotal;
		this.fechaSolicitud = fechaSolicitud;
		this.fechaEntrega = fechaEntrega;
		this.producto = producto;
		this.estadoEntrega = estadoEntrega;
	}
	
	public Pedido(int precioTotal, String fechaSolicitud, String fechaEntrega, Producto producto,
			boolean estadoEntrega) {
		
		this.precioTotal = precioTotal;
		this.fechaSolicitud = fechaSolicitud;
		this.fechaEntrega = fechaEntrega;
		this.producto = producto;
		this.estadoEntrega = estadoEntrega;
	}


	public int getPrecioTotal() {
		return precioTotal;
	}
	
	
	public void setPrecioTotal(int precioTotal) {
		this.precioTotal = precioTotal;
	}
	public String getFechaSolicitud() {
		return fechaSolicitud;
	}
	public void setFechaSolicitud(String fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}
	public String getFechaEntrega() {
		return fechaEntrega;
	}
	public void setFechaEntrega(String fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}
	public Producto getProducto() {
		return producto;
	}
	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	public boolean isEstadoEntrega() {
		return estadoEntrega;
	}
	public void setEstadoEntrega(boolean estadoEntrega) {
		this.estadoEntrega = estadoEntrega;
	}
	
	
}
