package eFoodSabana;

public class Producto {
private String Nombre;
private int Precio;
private String Descripcion;
private int Cantidad;
private int Cambio;

public String getNombre() {
	return Nombre;
}
public void setNombre(String nombre) {
	Nombre = nombre;
}
public int getPrecio() {
	return Precio;
}
public void setPrecio(int precio) {
	Precio = precio;
}
public String getDescripcion() {
	return Descripcion;
}
public void setDescripcion(String descripcion) {
	Descripcion = descripcion;
}
public int getCantidad() {
	return Cantidad;
}
public void setCantidad(int cantidad) {
	Cantidad = cantidad;
}


}
