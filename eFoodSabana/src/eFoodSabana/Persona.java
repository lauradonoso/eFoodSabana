package eFoodSabana;


import java.util.List;

public class Persona {
	
	private String nombres;
	private String apellidos;
	private String codigo;
	private Cuenta cuenta;
	public List<Pedido> pedido;
	
	public String getNombres() {
	 	 return nombres; 
	}
	
	public void setNombres(String nombres) { 
		 this.nombres = nombres; 
	}

	public String getApellidos() {
	 	 return apellidos; 
	}

	public void setApellidos(String apellidos) { 
		 this.apellidos = apellidos; 
	}

	public String getCodigo() {
	 	 return codigo; 
	}

	public void setCodigo(String codigo) { 
		 this.codigo = codigo; 
	}

	public Cuenta getCuenta() {
	 	 return cuenta; 
	}

	public void setCuenta(Cuenta cuenta) { 
		 this.cuenta = cuenta; 
	}

	public List<Pedido> getPedido() {
	 	 return pedido; 
	}

	public void setPedido(List<Pedido> pedido) { 
		 this.pedido = pedido; 
	}

	public Persona() { 
		// TODO Auto-generated method
	 }

	public Persona(String nombres, String apellidos, String codigo) { 
		// TODO Auto-generated method
	 }

	public Persona BuscarPersona(Cuenta cuenta) { 
		// TODO Auto-generated method
		return null;
	 } 

}