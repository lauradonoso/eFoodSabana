package eFoodSabana;

public class Pago {
	private String fecha;
	private Persona persona;
	private Pedido pedido;
	private String codigo;
	private String codigoVerificacion;
	
	public Pago(String fecha, Persona persona, Pedido pedido, String codigo,
			String codigoVerificacion) {
		super();
		this.fecha = fecha;
		this.persona = persona;
		this.pedido = pedido;
		this.codigo = codigo;
		this.codigoVerificacion = codigoVerificacion;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigoVerificacion() {
		return codigoVerificacion;
	}

	public void setCodigoVerificacion(String codigoVerificacion) {
		this.codigoVerificacion = codigoVerificacion;
	}
	
	
}
